<?php
/**
 * This file is part of the "rico_directory" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

defined('TYPO3_MODE') || die();

(function ($extensionKey) {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        "Riconet.$extensionKey",
        \Riconet\RicoDirectory\Constants::PLUGIN_NAME_DIRECTORY,
        [
            \Riconet\RicoDirectory\Controller\EntryController::NAME =>
                implode(',', \Riconet\RicoDirectory\Controller\EntryController::ACTIONS),
        ],
        [
            \Riconet\RicoDirectory\Controller\EntryController::NAME =>
                implode(',', \Riconet\RicoDirectory\Controller\EntryController::NON_CACHEABLE_ACTIONS),
        ]
    );
    // Add upgrade wizards.
    $wizards = [
        \Riconet\RicoDirectory\Updates\SlugifyUpdateWizard::class
    ];
    foreach ($wizards as $class) {
        $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/install']['update'][$class] = $class;
    }
})(\Riconet\RicoDirectory\Constants::EXTENSION_KEY);
