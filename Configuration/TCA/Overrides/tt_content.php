<?php

/**
 * This file is part of the "rico_directory" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

defined('TYPO3_MODE') || exit();

(function ($extensionKey) {
    // Register plugin.
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        "Riconet.$extensionKey",
        \Riconet\RicoDirectory\Constants::PLUGIN_NAME_DIRECTORY,
        'LLL:EXT:' . $extensionKey . '/Resources/Private/Language/locallang_db.xlf:tx_ricodirectory_directory'
    );

    // Add flex form for plugin directory.
    \Riconet\RicoDirectory\Utility\PluginUtility::registerFlexForm(
        $extensionKey,
        'Configuration/FlexForms/Directory.xml',
        \Riconet\RicoDirectory\Constants::PLUGIN_NAME_DIRECTORY
    );
})(\Riconet\RicoDirectory\Constants::EXTENSION_KEY);
