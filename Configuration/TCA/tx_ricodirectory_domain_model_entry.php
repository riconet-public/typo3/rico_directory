<?php

/**
 * This file is part of the "rico_directory" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

return (function ($extensionKey, $table) {
    /** @var \TYPO3\CMS\Core\Configuration\ExtensionConfiguration $extensionConfiguration */
    $extensionConfiguration = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
        \TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class
    );
    $ll = "LLL:EXT:$extensionKey/Resources/Private/Language/locallang_db.xlf";
    $fields = implode(',', [
        'title',
        'path_segment',
        'city',
        'street',
        'zip',
        'phone_number',
        'fax_number',
        'contact',
        'email',
        'description',
        'short_description',
        'key_words',
        'homepage',
        'categories',
        'images',
    ]);

    return [
        'ctrl' => [
            'title' => "$ll:$table",
            'label' => 'title',
            'tstamp' => 'tstamp',
            'crdate' => 'crdate',
            'cruser_id' => 'cruser_id',
            'languageField' => 'sys_language_uid',
            'transOrigPointerField' => 'l10n_parent',
            'transOrigDiffSourceField' => 'l10n_diffsource',
            'delete' => 'deleted',
            'enablecolumns' => [
                'disabled' => 'hidden',
                'starttime' => 'starttime',
                'endtime' => 'endtime',
            ],
            'searchFields' => $fields,
            'iconfile' => "EXT:$extensionKey/Resources/Public/Icons/$table.svg",
            'default_sortby' => 'title',
        ],
        'types' => [
            '1' => [
                'showitem' => implode(',', [
                    '--palette--;;paletteHidden',
                    '--palette--;;paletteLanguage',
                    $fields,
                    '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access',
                    '--palette--;;paletteAccess',
                ]),
            ],
        ],
        'palettes' => [
            'paletteHidden' => [
                'showitem' => 'hidden,',
            ],
            'paletteLanguage' => [
                'showitem' => 'sys_language_uid;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:sys_language_uid_formlabel,l10n_parent, l10n_diffsource,--palette--;;paletteAccess',
            ],
            'paletteAccess' => [
                'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access',
                'showitem' => implode(',', [
                    'starttime;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:starttime_formlabel',
                    'endtime;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:endtime_formlabel',
                ]),
            ],
        ],
        'columns' => [
            'sys_language_uid' => [
                'exclude' => true,
                'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
                'config' => [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'special' => 'languages',
                    'items' => [
                        [
                            'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages',
                            -1,
                            'flags-multiple',
                        ],
                    ],
                    'default' => 0,
                ],
            ],
            'l10n_parent' => [
                'displayCond' => 'FIELD:sys_language_uid:>:0',
                'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
                'config' => [
                    'type' => 'group',
                    'internal_type' => 'db',
                    'allowed' => $table,
                    'size' => 1,
                    'maxitems' => 1,
                    'minitems' => 0,
                    'default' => 0,
                ],
            ],
            'l10n_diffsource' => [
                'config' => [
                    'type' => 'passthrough',
                ],
            ],
            'hidden' => [
                'exclude' => true,
                'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.hidden',
                'config' => [
                    'type' => 'check',
                    'renderType' => 'checkboxToggle',
                    'default' => 0,
                    'items' => [
                        [
                            0 => '',
                            1 => '',
                        ],
                    ],
                ],
            ],
            'starttime' => [
                'exclude' => true,
                'l10n_mode' => 'mergeIfNotBlank',
                'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
                'config' => [
                    'type' => 'input',
                    'renderType' => 'inputDateTime',
                    'eval' => 'datetime',
                ],
            ],
            'endtime' => [
                'exclude' => true,
                'l10n_mode' => 'mergeIfNotBlank',
                'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
                'config' => [
                    'type' => 'input',
                    'renderType' => 'inputDateTime',
                    'eval' => 'datetime',
                ],
            ],
            'title' => [
                'exclude' => true,
                'label' => "$ll:$table.title",
                'config' => [
                    'type' => 'input',
                    'size' => 30,
                    'eval' => 'trim,required',
                ],
            ],
            'path_segment' => [
                'exclude' => true,
                'label' => "$ll:$table.path_segment",
                'config' => [
                    'type' => 'slug',
                    'generatorOptions' => [
                        'fields' => ['title'],
                        'replacements' => [
                            '/' => '-',
                        ],
                    ],
                    'fallbackCharacter' => '-',
                    'eval' => (string) $extensionConfiguration->get($extensionKey, 'slugBehaviour'),
                    'default' => '',
                ],
            ],
            'city' => [
                'exclude' => true,
                'label' => "$ll:$table.city",
                'config' => [
                    'type' => 'input',
                    'size' => 30,
                    'eval' => 'trim,required',
                ],
            ],
            'street' => [
                'exclude' => true,
                'label' => "$ll:$table.street",
                'config' => [
                    'type' => 'input',
                    'size' => 30,
                    'eval' => 'trim,required',
                ],
            ],
            'zip' => [
                'exclude' => true,
                'label' => "$ll:$table.zip",
                'config' => [
                    'type' => 'input',
                    'size' => 30,
                    'eval' => 'trim,required',
                ],
            ],
            'phone_number' => [
                'exclude' => true,
                'label' => "$ll:$table.phone_number",
                'config' => [
                    'type' => 'input',
                    'size' => 30,
                    'eval' => 'trim,required',
                ],
            ],
            'fax_number' => [
                'exclude' => true,
                'label' => "$ll:$table.fax_number",
                'config' => [
                    'type' => 'input',
                    'size' => 30,
                    'eval' => 'trim',
                ],
            ],
            'contact' => [
                'exclude' => true,
                'label' => "$ll:$table.contact",
                'config' => [
                    'type' => 'input',
                    'size' => 30,
                    'eval' => 'trim,required',
                ],
            ],
            'email' => [
                'exclude' => true,
                'label' => "$ll:$table.email",
                'config' => [
                    'type' => 'input',
                    'size' => 30,
                    'eval' => 'trim',
                ],
            ],
            'description' => [
                'exclude' => true,
                'label' => "$ll:$table.description",
                'config' => [
                    'type' => 'text',
                    'cols' => 40,
                    'rows' => 6,
                ],
                'defaultExtras' => 'richtext[]',
            ],
            'short_description' => [
                'exclude' => true,
                'label' => "$ll:$table.short_description",
                'config' => [
                    'type' => 'text',
                    'cols' => 40,
                    'rows' => 3,
                ],
                'defaultExtras' => 'richtext[]',
            ],
            'key_words' => [
                'exclude' => true,
                'label' => "$ll:$table.key_words",
                'config' => [
                    'type' => 'input',
                    'size' => 30,
                    'eval' => 'trim,required',
                ],
            ],
            'homepage' => [
                'exclude' => true,
                'label' => "$ll:$table.homepage",
                'config' => [
                    'type' => 'input',
                    'size' => 30,
                    'eval' => 'trim',
                ],
            ],
            'categories' => [
                'exclude' => true,
                'l10n_mode' => 'exclude',
                'l10n_display' => 'defaultAsReadonly',
                'label' => "$ll:$table.categories",
                'config' => [
                    'type' => 'select',
                    'renderType' => 'selectTree',
                    'treeConfig' => [
                        'parentField' => 'parent',
                        'appearance' => [
                            'showHeader' => true,
                            'expandAll' => false,
                            'maxLevels' => 99,
                        ],
                    ],
                    'MM' => 'sys_category_record_mm',
                    'MM_match_fields' => [
                        'fieldname' => 'categories',
                        'tablenames' => "$table",
                    ],
                    'MM_opposite_field' => 'items',
                    'foreign_table' => 'sys_category',
                    'foreign_table_where' => 'AND sys_category.sys_language_uid IN (0,-1)',
                    'size' => 10,
                    'minitems' => 0,
                    'maxitems' => 99,
                ],
            ],
            'images' => [
                'exclude' => true,
                'label' => "$ll:$table.images",
                'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                    'images',
                    [
                        'behaviour' => [
                            'allowLanguageSynchronization' => true,
                        ],
                        'appearance' => [
                            'createNewRelationLinkTitle' => 'LLL:EXT:frontend/locallang_ttc.xlf:images.addFileReference',
                            'showPossibleLocalizationRecords' => true,
                            'showRemovedLocalizationRecords' => true,
                            'showAllLocalizationLink' => true,
                            'showSynchronizationLink' => true,
                        ],
                        'foreign_match_fields' => [
                            'fieldname' => 'images',
                            'tablenames' => $table,
                            'table_local' => 'sys_file',
                        ],
                        'overrideChildTca' => [
                            'types' => [
                                \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                                    'showitem' => '
                                    --palette--;;imageoverlayPalette,
                                    --palette--;;filePalette',
                                ],
                            ],
                        ],
                    ],
                    $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
                ),
            ],
        ],
    ];
})(
    \Riconet\RicoDirectory\Constants::EXTENSION_KEY,
    'tx_ricodirectory_domain_model_entry'
);
