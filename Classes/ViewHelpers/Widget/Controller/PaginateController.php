<?php

/**
 * This file is part of the "rico_directory" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

namespace Riconet\RicoDirectory\ViewHelpers\Widget\Controller;

class PaginateController extends \TYPO3\CMS\Fluid\ViewHelpers\Widget\Controller\PaginateController
{
    /**
     * @var array
     */
    protected $filterArguments = [];

    public function initializeAction(): void
    {
        parent::initializeAction();
        $this->filterArguments = $this->widgetConfiguration['filterArguments'];
    }

    /**
     * @param int $currentPage
     */
    public function indexAction($currentPage = 1): void
    {
        parent::indexAction($currentPage);
        $this->view->assign('filterArguments', $this->filterArguments);
    }
}
