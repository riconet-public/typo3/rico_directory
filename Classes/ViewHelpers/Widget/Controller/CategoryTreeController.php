<?php

/**
 * This file is part of the "rico_directory" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

namespace Riconet\RicoDirectory\ViewHelpers\Widget\Controller;

use InvalidArgumentException;
use TYPO3\CMS\Extbase\Domain\Model\Category;
use TYPO3\CMS\Extbase\Domain\Repository\CategoryRepository;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Fluid\Core\Widget\AbstractWidgetController;

class CategoryTreeController extends AbstractWidgetController
{
    /**
     * @var CategoryRepository
     */
    protected $categoryRepository;

    /**
     * @var Category
     */
    protected $parent;

    /**
     * @var string
     */
    protected $outputMode = '';

    /**
     * @var string
     */
    protected $selectName = '';

    /**
     * @var array
     */
    protected $categoryTree = [];

    public function injectCategoryRepository(CategoryRepository $categoryRepository): void
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function initializeAction(): void
    {
        $this->validateArguments();
        $this->buildCategoryTreeArray($this->parent, $this->categoryTree);
    }

    public function indexAction(): void
    {
        $this->view->assignMultiple([
            'tree' => $this->categoryTree[$this->parent->getUid()],
            'outputMode' => $this->outputMode,
        ]);
        if ('select' === $this->outputMode) {
            $this->view->assign('selectName', $this->selectName);
        }
    }

    protected function getChildren(Category $parent): array
    {
        $result = $this->categoryRepository->findByParent($parent);
        if (!$result instanceof QueryResultInterface) {
            return [];
        }
        $children = $result->toArray();

        return count($children) > 0 ? $children : [];
    }

    protected function buildCategoryTreeArray(Category $parent, array &$tree): void
    {
        $children = $this->getChildren($parent);
        $tree[$parent->getUid()] = ['parent' => $parent];
        if (count($children) > 0) {
            $tree[$parent->getUid()] = [
                'parent' => $parent,
                'children' => [],
            ];
            foreach ($children as $child) {
                $this->buildCategoryTreeArray(
                    $child,
                    $tree[$parent->getUid()]['children']
                );
            }
        }
    }

    protected function validateArguments(): void
    {
        if (!($this->widgetConfiguration['parent'] instanceof Category)) {
            throw new InvalidArgumentException('The view helper "' . get_class($this) . '" only accepts as argument "\TYPO3\CMS\Extbase\Domain\Model\Category". ' . 'Given: ' . get_class($this->widgetConfiguration['parent']), 1385547291);
        }
        $this->parent = $this->widgetConfiguration['parent'];
        if ('ul' != $this->widgetConfiguration['outputMode'] && 'select' != $this->widgetConfiguration['outputMode']) {
            throw new InvalidArgumentException('The view helper "' . get_class($this) . '" accepts as argument "outputMode" value "ul" or "select". ' . 'given: "' . $this->widgetConfiguration['outputMode'] . '".', 1385547291);
        }
        $this->outputMode = $this->widgetConfiguration['outputMode'];
        $this->selectName = $this->widgetConfiguration['selectName'];
    }
}
