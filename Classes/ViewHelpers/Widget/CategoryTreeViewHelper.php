<?php

/**
 * This file is part of the "rico_directory" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

namespace Riconet\RicoDirectory\ViewHelpers\Widget;

use Riconet\RicoDirectory\ViewHelpers\Widget\Controller\CategoryTreeController;
use TYPO3\CMS\Extbase\Domain\Model\Category;
use TYPO3\CMS\Extbase\Mvc\ResponseInterface;
use TYPO3\CMS\Fluid\Core\Widget\AbstractWidgetViewHelper;

/**
 * Usage:
 *
 * {namespace r=Riconet\RicoDirectory\ViewHelpers}
 *
 * <r:widget.categoryTree parent="{category}" outputMode="ul" />
 */
class CategoryTreeViewHelper extends AbstractWidgetViewHelper
{
    /**
     * @var CategoryTreeController
     */
    protected $controller;

    public function injectCategoryTreeController(CategoryTreeController $controller): void
    {
        $this->controller = $controller;
    }

    public function initializeArguments(): void
    {
        parent::initializeArguments();
        $this->registerArgument(
            'parent',
            Category::class,
            'Root category for the tree.',
            true
        );
        $this->registerArgument(
            'outputMode',
            'string',
            'The wrapper tag.',
            false,
            'ul'
        );
    }

    public function render(): ResponseInterface
    {
        return $this->initiateSubRequest();
    }
}
