<?php

/**
 * This file is part of the "rico_directory" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

namespace Riconet\RicoDirectory\ViewHelpers\Widget;

use Riconet\RicoDirectory\ViewHelpers\Widget\Controller\PaginateController;

class PaginateViewHelper extends \TYPO3\CMS\Fluid\ViewHelpers\Widget\PaginateViewHelper
{
    public function __construct(PaginateController $controller)
    {
        $this->controller = $controller;
    }

    public function injectPaginateController(
        \TYPO3\CMS\Fluid\ViewHelpers\Widget\Controller\PaginateController $controller
    ): void {
        // Ignore injection. Otherwise the constructor injected controller would get overridden by this method.
    }

    public function initializeArguments(): void
    {
        parent::initializeArguments();
        $this->registerArgument(
            'filterArguments',
            'array',
            'FilterArguments',
            true
        );
    }
}
