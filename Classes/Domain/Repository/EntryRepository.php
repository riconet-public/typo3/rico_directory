<?php

/**
 * This file is part of the "rico_directory" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

namespace Riconet\RicoDirectory\Domain\Repository;

use Riconet\RicoDirectory\Filter\FilterArguments;
use TYPO3\CMS\Extbase\Domain\Model\Category;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;

class EntryRepository extends Repository
{
    protected $defaultOrderings = [
        'title' => QueryInterface::ORDER_ASCENDING,
    ];

    /**
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface|null
     */
    public function findByFilterArguments(FilterArguments $filterArguments = null)
    {
        $query = $this->createQuery();
        // Return all, if no filterArguments are set.
        if (is_null($filterArguments)) {
            return $this->findAll();
        }
        $constrains = [];
        // Filter by searchText.
        if (!empty($filterArguments->getSearchText())) {
            $constrains[] = $query->like('title', '%' . $filterArguments->getSearchText() . '%');
        }
        // Filter by category.
        if ($filterArguments->getCategory() instanceof Category) {
            $constrains[] = $query->contains('categories', $filterArguments->getCategory());
        }
        // Check the constrain.
        if (count($constrains) > 0) {
            return $query->matching(
                $query->logicalAnd(
                    $query->logicalAnd($constrains)
                )
            )->execute();
        }

        return $this->findAll();
    }

    public function findByCharacter(string $character): array
    {
        $query = $this->createQuery();
        $query->matching(
            $query->like('title', $character . '%')
        );
        $result = $query->execute();

        return $result instanceof QueryResultInterface ? $result->toArray() : $result;
    }

    public function findByCategory(Category $category): array
    {
        $query = $this->createQuery();
        $query->matching(
            $query->contains('categories', $category)
        );
        $result = $query->execute();

        return $result instanceof QueryResultInterface ? $result->toArray() : $result;
    }
}
