<?php

/**
 * This file is part of the "rico_directory" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

namespace Riconet\RicoDirectory\Domain\Manager;

use Doctrine\DBAL\Driver\Statement;
use PDO;
use Riconet\RicoDirectory\Domain\Service\SlugifyServiceInterface;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;

class DirectorySlugManager implements DirectorySlugManagerInterface
{
    private const TABLE = 'tx_ricodirectory_domain_model_entry';
    private const SLUG_FIELD = 'path_segment';

    /**
     * @var ConnectionPool
     */
    protected $connectionPool;

    /**
     * @var SlugifyServiceInterface
     */
    private $slugifyService;

    public function __construct(ConnectionPool $connectionPool, SlugifyServiceInterface $slugifyService)
    {
        $this->connectionPool = $connectionPool;
        $this->slugifyService = $slugifyService;
    }

    public function getEmptySlugCount(): int
    {
        $queryBuilder = $this->getPreparedQueryBuilder();
        $result = $queryBuilder->count('uid')->execute();

        return (int) ($result instanceof Statement ? $result->fetchColumn(0) : 0);
    }

    public function fillEmptySlugs(): void
    {
        $queryBuilder = $this->getPreparedQueryBuilder();
        $result = $queryBuilder->select('uid')->execute();
        $result = $result instanceof Statement ? $result->fetchAll() : [];
        foreach ($result as $record) {
            $slug = $this->slugifyService->slugifyRecord((int) $record['uid'], self::TABLE, self::SLUG_FIELD);
            $this->updateRecordSlug((int) $record['uid'], $slug);
        }
    }

    protected function getPreparedQueryBuilder(): QueryBuilder
    {
        $queryBuilder = $this->connectionPool->getQueryBuilderForTable(self::TABLE);
        $queryBuilder->getRestrictions()->removeAll();
        $queryBuilder->from(self::TABLE)
            ->where(
                $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->eq(
                        self::SLUG_FIELD,
                        $queryBuilder->createNamedParameter('', PDO::PARAM_STR)
                    ),
                    $queryBuilder->expr()->isNull(self::SLUG_FIELD)
                )
            );

        return $queryBuilder;
    }

    protected function updateRecordSlug(int $uid, string $slug): void
    {
        $queryBuilder = $this->connectionPool->getQueryBuilderForTable(self::TABLE);
        $queryBuilder->getRestrictions()->removeAll();
        $queryBuilder->update(self::TABLE)
            ->where($queryBuilder->expr()->eq('uid', ':uid'))
            ->set(self::SLUG_FIELD, $slug)
            ->setParameter('uid', $uid, PDO::PARAM_INT)
            ->execute();
    }
}
