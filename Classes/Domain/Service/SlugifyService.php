<?php

/**
 * This file is part of the "rico_directory" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

namespace Riconet\RicoDirectory\Domain\Service;

use Doctrine\DBAL\Driver\Statement;
use InvalidArgumentException;
use Riconet\RicoDirectory\DataHandling\SlugHelper;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\DataHandling\Model\RecordState;
use TYPO3\CMS\Core\DataHandling\Model\RecordStateFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class SlugifyService implements SlugifyServiceInterface
{
    /**
     * @var ConnectionPool
     */
    protected $connectionPool;

    /**
     * @var SlugHelper
     */
    protected $slugHelper;

    public function __construct(ConnectionPool $connectionPool, SlugHelper $slugHelper)
    {
        $this->connectionPool = $connectionPool;
        $this->slugHelper = $slugHelper;
    }

    public function slugifyRecord(int $uid, string $table, string $slugField): string
    {
        $record = $this->fetchRecord($uid, $table);

        // Get TCA field configuration.
        $fieldConfig = (array) $GLOBALS['TCA'][$table]['columns'][$slugField]['config'];
        $evalInfo = GeneralUtility::trimExplode(',', $fieldConfig['eval'], true);

        $this->slugHelper->setup($table, $slugField, $fieldConfig);
        $recordState = $this->getRecordState($table, $record);

        return $this->generateSlug($recordState, $record, $evalInfo);
    }

    protected function fetchRecord(int $uid, string $table): array
    {
        $queryBuilder = $this->getQueryBuilder($table);
        $queryBuilder->getRestrictions()->removeAll();
        $statement = $queryBuilder
            ->select('*')
            ->from($table)
            ->where($queryBuilder->expr()->eq('uid', ':uid'))
            ->setParameter(':uid', $uid, Connection::PARAM_INT)
            ->execute();
        if (!$statement instanceof Statement) {
            throw new InvalidArgumentException('Invalid query.');
        }
        $record = $statement->fetch();
        if (false === $record) {
            throw new InvalidArgumentException("Could not fetch record of table $table with uid $uid.");
        }

        return $record;
    }

    protected function getQueryBuilder(string $table): QueryBuilder
    {
        return $this->connectionPool->getConnectionForTable($table)->createQueryBuilder();
    }

    protected function getRecordState(string $table, array $record): RecordState
    {
        return RecordStateFactory::forName($table)->fromArray($record, $record['pid'], $record['uid']);
    }

    protected function generateSlug(RecordState $recordState, array $record, array $evalInfo): string
    {
        $slug = $this->slugHelper->generate($record, $record['pid']);

        // Build slug depending on eval configuration.
        if (in_array('uniqueInSite', $evalInfo)) {
            return $this->slugHelper->buildSlugForUniqueInSite($slug, $recordState);
        }
        if (in_array('uniqueInPid', $evalInfo)) {
            return $this->slugHelper->buildSlugForUniqueInPid($slug, $recordState);
        }
        if (in_array('unique', $evalInfo)) {
            return $this->slugHelper->buildSlugForUniqueInTable($slug, $recordState);
        }

        return $slug;
    }
}
