<?php

/**
 * This file is part of the "rico_directory" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

namespace Riconet\RicoDirectory\Domain\Model;

use TYPO3\CMS\Extbase\Annotation\ORM\Lazy;
use TYPO3\CMS\Extbase\Annotation\Validate;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

class Entry extends AbstractEntity
{
    /**
     * @var string|null
     *
     * @Validate("NotEmpty")
     */
    protected $title;

    /**
     * @var string|null
     *
     * @Validate("NotEmpty")
     */
    protected $city;

    /**
     * @var string|null
     *
     * @Validate("NotEmpty")
     */
    protected $street;

    /**
     * @var string|null
     *
     * @Validate("NotEmpty")
     */
    protected $zip;

    /**
     * @var string|null
     *
     * @Validate("NotEmpty")
     */
    protected $phoneNumber;

    /**
     * @var string|null
     */
    protected $faxNumber;

    /**
     * @var string|null
     *
     * @Validate("NotEmpty")
     */
    protected $contact;

    /**
     * @var string|null
     */
    protected $email;

    /**
     * @var string|null
     */
    protected $description;

    /**
     * @var string|null
     */
    protected $shortDescription;

    /**
     * @var string|null
     *
     * @Validate("NotEmpty")
     */
    protected $keyWords;

    /**
     * @var string|null
     */
    protected $homepage;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\Category>|null
     *
     * @Lazy()
     */
    protected $categories;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>|null
     *
     * Lazy()
     */
    protected $images;

    public function __construct()
    {
        $this->initStorageObjects();
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): void
    {
        $this->city = $city;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(?string $street): void
    {
        $this->street = $street;
    }

    public function getZip(): ?string
    {
        return $this->zip;
    }

    public function setZip(?string $zip): void
    {
        $this->zip = $zip;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?string $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

    public function getFaxNumber(): ?string
    {
        return $this->faxNumber;
    }

    public function setFaxNumber(?string $faxNumber): void
    {
        $this->faxNumber = $faxNumber;
    }

    public function getContact(): ?string
    {
        return $this->contact;
    }

    public function setContact(?string $contact): void
    {
        $this->contact = $contact;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getShortDescription(): ?string
    {
        return $this->shortDescription;
    }

    public function setShortDescription(?string $shortDescription): void
    {
        $this->shortDescription = $shortDescription;
    }

    public function getKeyWords(): ?string
    {
        return $this->keyWords;
    }

    public function setKeyWords(?string $keyWords): void
    {
        $this->keyWords = $keyWords;
    }

    public function getHomepage(): ?string
    {
        return $this->homepage;
    }

    public function setHomepage(?string $homepage): void
    {
        $this->homepage = $homepage;
    }

    public function getCategories(): ?ObjectStorage
    {
        return $this->categories;
    }

    public function setCategories(?ObjectStorage $categories): void
    {
        $this->categories = $categories;
    }

    public function getImages(): ?ObjectStorage
    {
        return $this->images;
    }

    public function setImages(?ObjectStorage $images): void
    {
        $this->images = $images;
    }

    protected function initStorageObjects(): void
    {
        $this->categories = new ObjectStorage();
        $this->images = new ObjectStorage();
    }
}
