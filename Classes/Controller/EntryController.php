<?php

/**
 * This file is part of the "rico_directory" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

namespace Riconet\RicoDirectory\Controller;

use Riconet\RicoDirectory\Domain\Model\Entry;
use Riconet\RicoDirectory\Domain\Repository\EntryRepository;
use Riconet\RicoDirectory\Filter\FilterArguments;
use TYPO3\CMS\Extbase\Domain\Model\Category;
use TYPO3\CMS\Extbase\Domain\Repository\CategoryRepository;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

class EntryController extends ActionController
{
    public const NAME = 'Entry';

    public const ACTIONS = ['index', 'character', 'category', 'show'];

    public const NON_CACHEABLE_ACTIONS = ['index'];

    private const CHARACTERS = [
        'A',
        'B',
        'C',
        'D',
        'E',
        'F',
        'G',
        'H',
        'I',
        'J',
        'K',
        'L',
        'M',
        'N',
        'O',
        'P',
        'Q',
        'R',
        'S',
        'T',
        'U',
        'V',
        'W',
        'X',
        'Y',
        'Z',
        'Ä',
        'Ö',
        'Ü',
    ];

    /**
     * @var EntryRepository
     */
    protected $entryRepository;

    /**
     * @var CategoryRepository
     */
    protected $categoryRepository;

    /**
     * @var Category|null
     */
    protected $parentCategory;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult
     */
    protected $possibleCategories;

    public function injectEntryRepository(EntryRepository $entryRepository): void
    {
        $this->entryRepository = $entryRepository;
    }

    public function injectCategoryRepository(CategoryRepository $categoryRepository): void
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function initializeAction(): void
    {
        /** @var Category|null $parentCategory */
        $parentCategory = $this->categoryRepository->findByUid((int) $this->settings['parentCategoryUid']);
        $this->parentCategory = $parentCategory;
        // Extract filterArguments from pagination, if set.
        if (!$this->request->hasArgument('@widget_0')) {
            return;
        }
        $widget = (array) $this->request->getArgument('@widget_0');
        if (!isset($widget['arguments']) || empty($widget['arguments'])) {
            return;
        }
        $this->request->setArgument('filterArguments', $widget['arguments']);
        $propertyMappingConfiguration =
            $this->arguments->getArgument('filterArguments')->getPropertyMappingConfiguration();
        $propertyMappingConfiguration->allowAllProperties();
    }

    public function indexAction(FilterArguments $filterArguments = null): void
    {
        $entries = $this->entryRepository->findByFilterArguments($filterArguments);
        $categories = $this->categoryRepository->findByParent($this->parentCategory);
        $this->view->assignMultiple([
            'entries' => $entries,
            'categories' => $categories,
            'parentCategory' => $this->parentCategory,
            'characters' => self::CHARACTERS,
            'filterArguments' => $filterArguments,
            'urlEncodebleFilterArguments' => $filterArguments instanceof FilterArguments ?
                $filterArguments->toArray() : null,
        ]);
    }

    public function showAction(Entry $entry): void
    {
        $this->view->assign('entry', $entry);
    }

    public function characterAction(string $character): void
    {
        $this->view->assignMultiple([
            'entries' => $this->entryRepository->findByCharacter($character),
            'parentCategory' => $this->parentCategory,
            'characters' => self::CHARACTERS,
        ]);
    }

    public function categoryAction(): void
    {
        if (!$this->request->hasArgument('category')) {
            $this->redirect('index');
        }
        $uid = intval($this->request->getArgument('category'));
        /** @var Category $category */
        $category = $this->categoryRepository->findByUid($uid);
        $this->view->assignMultiple([
            'entries' => $this->entryRepository->findByCategory($category),
            'category' => $category,
            'parentCategory' => $this->parentCategory,
            'characters' => self::CHARACTERS,
        ]);
    }
}
