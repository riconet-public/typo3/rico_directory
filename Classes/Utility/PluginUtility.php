<?php

/**
 * This file is part of the "rico_directory" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

namespace Riconet\RicoDirectory\Utility;

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

class PluginUtility
{
    public static function registerFlexForm(string $extensionKey, string $rootRelativePath, string $pluginName): void
    {
        $pluginSignature = str_replace('_', '', $extensionKey) . '_' . mb_strtolower($pluginName);
        $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
        ExtensionManagementUtility::addPiFlexFormValue(
            $pluginSignature,
            'FILE:EXT:' . $extensionKey . '/' . $rootRelativePath
        );
    }
}
