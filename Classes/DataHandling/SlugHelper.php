<?php

/**
 * This file is part of the "rico_directory" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

namespace Riconet\RicoDirectory\DataHandling;

use TYPO3\CMS\Backend\Utility\BackendUtility;

/**
 * This class is used to allow dependency injecting of the base class.
 *
 * We override the default constructor. This allows us to create an instance of the class without passing the arguments.
 * But we need those later in the life cycle of the instance, so a setup method is provided to pass this data.
 */
class SlugHelper extends \TYPO3\CMS\Core\DataHandling\SlugHelper
{
    public function __construct(string $tableName = '', string $fieldName = '', array $configuration = [], int $workspaceId = 0)
    {
        parent::__construct($tableName, $fieldName, $configuration, $workspaceId);
    }

    public function setup(string $tableName, string $fieldName, array $configuration, int $workspaceId = 0): void
    {
        $this->tableName = $tableName;
        $this->fieldName = $fieldName;
        $this->configuration = $configuration;
        $this->workspaceId = $workspaceId;

        $this->prependSlashInSlug = $this->configuration['prependSlash'] ?? false;
        if ('pages' === $this->tableName && 'slug' === $this->fieldName) {
            $this->prependSlashInSlug = true;
        }

        $this->workspaceEnabled = BackendUtility::isTableWorkspaceEnabled($tableName);
    }
}
