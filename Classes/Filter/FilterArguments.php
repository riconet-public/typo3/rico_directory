<?php

/**
 * This file is part of the "rico_directory" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

namespace Riconet\RicoDirectory\Filter;

use TYPO3\CMS\Extbase\Domain\Model\Category;

class FilterArguments
{
    /**
     * @var string|null
     */
    protected $searchText;

    /**
     * @var \TYPO3\CMS\Extbase\Domain\Model\Category|null
     */
    protected $category;

    public function getSearchText(): ?string
    {
        return $this->searchText;
    }

    public function setSearchText(?string $searchText): void
    {
        $this->searchText = $searchText;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): void
    {
        $this->category = $category;
    }

    public function toArray(): array
    {
        return [
            'searchText' => $this->searchText ?? '',
            'category' => $this->category instanceof Category ? $this->category->getUid() : '',
        ];
    }
}
