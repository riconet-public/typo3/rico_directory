<?php
/**
 * This file is part of the "rico_directory" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

$EM_CONF[\Riconet\RicoDirectory\Constants::EXTENSION_KEY] = [
    'title' => 'Directory',
    'description' => 'This extension makes it possible to create directory entries like a company or a society',
    'version' => '3.1.1',
    'category' => 'plugin',
    'constraints' => [
        'depends' => [
            'typo3' => '10.4.0-10.4.99',
            'vhs' => '6.0.0-6.0.99',
        ]
    ],
    'state' => 'stable',
    'uploadfolder' => false,
    'clearCacheOnLoad' => true,
    'author' => 'Wolf Utz',
    'author_email' => 'w.utz@psv-neo.de',
    'author_company' => 'PSVneo',
    'autoload' => [
        'psr-4' => [
            'Riconet\\RicoDirectory\\' => 'Classes',
        ]
    ],
    'autoload-dev' => [
        'psr-4' => [
            'Riconet\\RicoDirectory\\Tests\\' => 'Tests',
        ]
    ],
];
